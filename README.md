## AsmodeosCore 

Es un Emulador de World Of Warcraft Cataclysm Compatible con el Build: 15595 
Basado en *Mangos* *TrinityCore* y *SkyfireEMU* el cual tiene como 
Desarrollador Principal a Sebastian Godoy y William Andres
*Dueño y Administrador* de Asmodeos Network 

## Requireientos

+ Platform: Linux o  Windows
+ Processor with SSE2 support
+ Boost ≥ 1.49
+ MySQL ≥ 5.1.0
+ CMake ≥ 2.8.11.2 / 2.8.9 (Windows / Linux)
+ OpenSSL ≥ 1.0.0
+ GCC ≥ 4.7.2 (Linux only)
+ MS Visual Studio ≥ 12 (2013) (Windows)


## Install

Para La instalacion se recomienda contactar con los Desarrolladores


## Reporting issues

Pueden Reportar errores en [Asmodeos Network Issues] (http://asmodeoswow.tk/home/bugtracker).

Porfavor tomese el tiempo de revisar los errores reportados previamente para evitar post duplicados
prevent duplicates.

##Licensee: GPL 2.0

Read file [COPYING](COPYING)


## Authors &amp; Contributors

Read file [THANKS](THANKS)


## Links

[Site](http://asmodeoswow.tk)

[Wiki](http://asmodeosnetwork.tk/)

[Documentation](http://www.trinitycore.net) (powered by Doxygen)

[Forums](http://asmodeoswow.tk/foro//)
